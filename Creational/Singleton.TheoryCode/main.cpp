#include <iostream>
#include <thread>
#include "singleton.hpp"

using namespace std;

atomic<bool> is_ready {false};
string data;

void use()
{
    while (!is_ready)
    {
    }

    cout << "processing " << data << endl;

    Singleton::instance().do_something();
}

int main()
{
    cout << "Start main..." << endl;

    thread thd{use};

	Singleton::instance().do_something();

    Singleton& singleObject = Singleton::instance();

    data = "text";
    is_ready = true;

	singleObject.do_something();

    Singleton::instance().do_something();

    thd.join();
}
