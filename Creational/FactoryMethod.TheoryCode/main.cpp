#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>

#include "factory.hpp"

using namespace std;

namespace Before
{
    class Client
    {

    public:
        Client()
        {
        }

        Client(const Client&) = delete;
        Client& operator=(const Client&) = delete;

        void use()
        {
            ConcreteServiceA service;

            string result = service.run();
            cout << "Client is using: " << result << endl;
        }
    };
}

class Client
{
    shared_ptr<ServiceCreator> creator_;
public:
    Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
	{
	}

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
	{
        unique_ptr<Service> service = creator_->create_service();

        string result = service->run();
        cout << "Client is using: " << result << endl;
	}
};

int main()
{
    typedef std::map<std::string, shared_ptr<ServiceCreator>> Factory;
    Factory creators;
    creators.insert(make_pair("CreatorA", make_shared<ConcreteCreatorA>()));
    creators.insert(make_pair("CreatorB", make_shared<ConcreteCreatorB>()));
    creators.insert(make_pair("CreatorC", make_shared<ConcreteCreatorC>()));

    Client client(creators["CreatorC"]);
	client.use();
}
