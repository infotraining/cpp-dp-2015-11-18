#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"

// TODO: Dodać klase Square
namespace Drawing
{
    class Square : public ShapeBase
    {
        int size_;
    public:
        Square(int x = 0, int y = 0, int size = 0) : ShapeBase(x, y), size_{size}
        {}

        void draw() const override
        {
            std::cout << "Drawing square at: " << point() << " with size: " << size_ << std::endl;
        }

        virtual void read(std::istream& in) override
        {
            Point pt;
            unsigned int size;

            in >> pt >> size;

            set_point(pt);
            size_ = size;
        }

        virtual void write(std::ostream& out) override
        {
            out << "Square " << point() << " " << size_ << std::endl;
        }
    };
}


#endif /* SQUARE_HPP_ */
