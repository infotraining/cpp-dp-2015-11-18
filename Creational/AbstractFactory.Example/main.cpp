#include "game.hpp"

using namespace Game;

int main()
{
	GameApp game;

    game.select_level(GameLevel::die_hard);
	game.init_game(12);
	game.test_monsters();

    std::cout << "\nChanging level...\n";

    game.select_level(GameLevel::easy);
    game.init_game(12);
    game.test_monsters();
}
