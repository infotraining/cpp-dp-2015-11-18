#ifndef TEXT_HPP
#define TEXT_HPP

#include <string>
#include "shape.hpp"
#include "paragraph.hpp"

namespace Drawing
{
// TODO - zaadaptowac klase Paragraph do wymogow klienta
    class Text : public ShapeBase
    {
        LegacyCode::Paragraph paragraph_;
    public:
        Text(int x = 0, int y = 0, const std::string& text = "")
            : ShapeBase(x, y), paragraph_(text.c_str())
        {
        }

        void draw() const override
        {
            paragraph_.render_at(point().x(), point().y());
        }

        void read(std::istream& in) override
        {
            Point pt;
            std::string text;

            in >> pt >> text;

            set_point(pt);
            paragraph_.set_paragraph(text.c_str());
        }

        void write(std::ostream& out) override
        {
            out << "Text " << point() << " " << paragraph_.get_paragraph() << std::endl;
        }

        Shape* clone() const override
        {
            return new Text(*this);
        }
    };
}

#endif
