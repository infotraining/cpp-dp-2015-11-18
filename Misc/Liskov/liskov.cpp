#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Shape
{
public:
	virtual void draw() const = 0;
	virtual ~Shape() = default;
};

class Rectangle : public Shape
{
	int w_, h_;
public:
	Rectangle(int w, int h) : w_{w}, h_{h}
	{}

	void set_w(int w)
	{
		w_ = w;
	}

	void set_h(int h)
	{
		h_ = h;
	}

	void draw() const override
	{
		cout << "Drawing rectangle: [ " << w_ << " " << h_ << "]\n";
	}
};

class Square : public Shape
{
	Rectangle r_;
public:
	Square(int size) : r_{size, size}
	{}

	void set_size(int size)
	{
		r_.set_w(size);
		r_.set_h(size);
	}

	void draw() const override
	{
		r_.draw();
	}
};

int main()
{
	vector<shared_ptr<Shape>> shapes;

	shapes.push_back(make_shared<Rectangle>(10, 20));
	shapes.push_back(make_shared<Square>(30));

	for(const auto& s : shapes)
		s->draw();
}