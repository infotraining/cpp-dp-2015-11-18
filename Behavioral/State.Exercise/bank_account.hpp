#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>
#include <sstream>

enum AccountState { OVERDRAFT, NORMAL };

class BankAccount
{
    int id_;
    double balance_;
    AccountState state_;
protected:
    void update_account_state()
    {
        if (balance_ < 0)
            state_ = OVERDRAFT;
        else
            state_ = NORMAL;
    }

    void set_balance(double amount)
    {
        balance_ = amount;
    }
public:
    BankAccount(int id) : id_(id), balance_(0.0), state_(NORMAL) {}

    void withdraw(double amount)
    {
        assert(amount > 0);

        if (state_ == OVERDRAFT)
        {
            std::cout << "Brak srodkow na koncie #" << id_ <<  std::endl;
        }
        else // state_ == NORMAL
        {
            balance_ -= amount;

            update_account_state();
        }
    }

    void deposit(double amount)
    {
        assert(amount > 0);

        balance_ += amount;

        update_account_state();
    }

    void pay_interest()
    {
        if (state_ == OVERDRAFT)
            balance_ += balance_ * 0.15;

        else
            balance_ += balance_ * 0.05;
    }

    std::string status() const
    {
        std::stringstream strm;
        strm << "BankAccount #" << id_ << "; State: ";

        if (state_ == OVERDRAFT)
            strm << "Overdraft; ";
        else
            strm << "Normal; ";

        strm << "Balance: " << balance_;

        return strm.str();
    }

    double balance() const
    {
        return balance_;
    }

    int id() const
    {
        return id_;
    }
};

#endif
